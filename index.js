require('./model/mongodb')
const courseController = require('./controllers/courseController');
const userController = require('./controllers/UserController');
const auth = require('./controllers/auth')
const authMiddleware = require('./middlewares/auth')
const config = require('config')
//Import the necessary packages
const express = require('express');

var app = express();
const bodyparser = require('body-parser');
 
app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());
 
 if(!config.get("jwtPrivateKey")){
    console.log('JWT PRIVATE KEY IS NOT DEFINED')
    process.exit(1)
} 
//Create a welcome message and direct them to the main page
app.get('/', (req, res) => {
    res.send('Welcome to our app');
});

//Set the Controller path which will be responding the user actions
app.use('/api/courses', authMiddleware, courseController);
app.use('/api/users',userController)
app.use('/api/auth',auth)
//Establish the server connection
//PORT ENVIRONMENT VARIABLE
const port = process.env.PORT || 8080;
app.listen(port, () => console.log(`Listening on port ${port}..`));
 